import Vue from 'vue'
import App from './App.vue'
import IsVisibleDirective from './Directives';

Vue.config.productionTip = false

Vue.directive('isVisible', IsVisibleDirective);

new Vue({
  render: h => h(App),
}).$mount('#app')
