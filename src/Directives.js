const IsVisibleDirective = {
  bind(el, binding) {
    el.style.visibility = (binding.value) ? "" : "hidden";
  },
  update(el, binding) {
    el.style.visibility = (binding.value) ? "" : "hidden";
  }
}

export default IsVisibleDirective;